import React from 'react';
import './App.css';
import Navbar from './components/nav/NavBar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/pages/home';
import AboutUs from './components/pages/aboutUs';
import ContactUs from './components/pages/contactUs';
import Chronicle from './components/pages/chronicle';
import SignUp from './components/pages/signUp';

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/about-us' component={AboutUs} />
        <Route path='/chronicle' component={Chronicle} />
        <Route path='/contact-us' component={ContactUs} />
        <Route path='/sign-up' component={SignUp} />
      </Switch>
    </Router>
  );
}

export default App;