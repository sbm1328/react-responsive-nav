import React from "react";
import {
  Nav,
  NavLink,
  Bars,
  NavMenu,
  NavBtn,
  NavBtnLink,
} from "./navBarElements";

const Navbar = () => {
  return (
    <>
      <Nav>
        <NavLink to="/">
          <img style={{width:'75px'}} src={require('../../logo.svg').default} alt="logo" />
        </NavLink>
        <Bars />
        <NavMenu>
          <NavLink to="/about-us" activeStyle>
            About
          </NavLink>
          <NavLink to="/chronicle" activeStyle>
            Chronicle
          </NavLink>
          <NavLink to="/contact-us" activeStyle>
            Contact Us
          </NavLink>
          <NavLink to="/sign-up" activeStyle>
            Sign Up
          </NavLink>
          {/* Second Nav */}
          {/* <NavBtnLink to='/sign-in'>Sign In</NavBtnLink> */}
        </NavMenu>
        <NavBtn>
          <NavBtnLink to="/signin">Sign In</NavBtnLink>
        </NavBtn>
      </Nav>
    </>
  );
};

export default Navbar;
