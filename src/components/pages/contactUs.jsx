import React from "react";
import { CustomBody } from "./customElements/bodyElements";

const ContactUs = () => {
  return (
    <CustomBody>
      <h1>Contact Us</h1>
    </CustomBody>
  );
};

export default ContactUs;
