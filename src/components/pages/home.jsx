import React from "react";
import { CustomBody } from "./customElements/bodyElements";

const Home = () => {
  return (
    <CustomBody>
      <h1>Home</h1>
    </CustomBody>
  );
};

export default Home;
