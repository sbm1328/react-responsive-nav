import React from "react";
import { CustomBody } from "./customElements/bodyElements";

const Chronicle = () => {
  return (
    <CustomBody>
      <h1>Chronicle</h1>
    </CustomBody>
  );
};

export default Chronicle;
