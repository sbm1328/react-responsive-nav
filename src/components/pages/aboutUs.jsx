import React from "react";
import { CustomBody } from "./customElements/bodyElements";

const AboutUs = () => {
  return (
    <CustomBody>
      <h1>About Us</h1>
    </CustomBody>
  );
};

export default AboutUs;
